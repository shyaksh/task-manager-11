package ru.bokhan.tm.api.repository;

import ru.bokhan.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
